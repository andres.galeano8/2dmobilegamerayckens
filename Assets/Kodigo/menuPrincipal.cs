using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuPrincipal : MonoBehaviour
{
    public GameObject soundOn;
    public GameObject soundOff;
    public AudioSource soundMenu;
    public float volumen;

    private void Start()
    {
        soundOff.SetActive(false);
        OpcionesDeJuego.volumenControl = soundMenu.volume;

    }

    public void PlayButton ()
    {
        SceneManager.LoadScene(1);
    }
    public void ExitButton ()
    {
        Application.Quit();
    }
    public void VolumenButton(int accion)
    {
        switch (accion)
        {
            case 1:
                soundMenu.volume=0;
                soundOn.SetActive(false);
                soundOff.SetActive(true);
                OpcionesDeJuego.volumenControl=soundMenu.volume;
                break;
            case 2:
                soundMenu.volume= 1;
                soundOn.SetActive(true);
                soundOff.SetActive(false);
                OpcionesDeJuego.volumenControl = soundMenu.volume;
                break;
            default:
                break;
        }
    }
}
