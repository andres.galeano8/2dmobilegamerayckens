using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Parametros De Interfaz Grafica
    [Header("Interfaz Grafica")]
    public GameObject menuGameOver;
    public GameObject menuPaused;
    public GameObject warningText;
    public GameObject allyWarning;
    public GameObject soundOn;
    public GameObject soundOff;
    public TMP_Text puntaje;
    public TMP_Text puntajePantallaGO;
    public TMP_Text vidaEnPantalla;
    public TMP_Text reloj;
    public AudioSource soundGame;
    public AudioSource soundMenu;
    public float points;
    public static bool estaPausado = false;
    
    //Parametros de Juego
    [Header("Parametros de Juego")]
    public static GameManager Instance;
    public GameObject ninja;
    public GameObject human;
    public GameObject bigEnemy;
    public Transform[] spawn;
    public float timeSpawn;
    public float timeSpawnAlly;
    public float timeSpawnBE;
    public float speedControler;
    private float privateTime;
    private float privateTimeAlly;
    private float privateTimeBE;
    public int currentLife;
    float controlTime = 0;
    float ts;
    float sc;
    //Reloj
    [Header("Reloj")]
    [Tooltip("Tiempo iniciar en Segundos")]
    public int tInicial;
    [Tooltip("Escala del Tiempo del Reloj")]
    [Range(-10.0f, 10.0f)]
    public float escalaDeTiempo = 1;
    private float tFrameTScale = 0f;
    private float tEnSegundos = 0F;
    private float escalatiempoPausa, escalaTInicial;
    //Codigo ----------------------------------------------------
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        ts = 30;
        sc = 1;
        privateTime = 0;
        currentLife = 3;
        speedControler = 1;
        //Reloj durante el juego
        escalaTInicial = escalaDeTiempo;
        tEnSegundos = tInicial;
        ActualizarReloj(tInicial);
        menuGameOver.SetActive(false);
        warningText.SetActive(false);
        Life(0);
        soundGame.volume = OpcionesDeJuego.volumenControl;
        soundOff.SetActive(false);

    }
    private void Update()
    {
        if (estaPausado)
        {
            return;
        }
        ControlDeOleadas();
        ControlDeDificultad();
        //Control de oleadas
        privateTime += Time.deltaTime;
        privateTimeAlly += Time.deltaTime;
        privateTimeBE += Time.deltaTime;
        //Reloj
        tFrameTScale = Time.deltaTime * escalaDeTiempo;
        tEnSegundos += tFrameTScale;
        ActualizarReloj(tEnSegundos);
        controlTime += Time.deltaTime;
    }
    void SpawnEnemy(float cantidad)
    {
        for (int i = 0; i < cantidad; i++)
        {
            GameObject go = Instantiate(ninja, spawn[Random.Range(0, spawn.Length)]);

            go.transform.localPosition = Vector2.zero;

            Debug.Log(i);

        }

    }
    void SpawnAlly (float amountAlly)
    {
        for (int i = 0; i < amountAlly; i++)
        {
            GameObject go = Instantiate(human, spawn[Random.Range(0, spawn.Length)]);

            go.transform.localPosition = Vector2.zero;

            Debug.Log(i);

        }
    }
    void SpawnBigEnemy(float enemys)
    {
        for (int i = 0; i < enemys; i++)
        {
            GameObject go = Instantiate(bigEnemy, spawn[Random.Range(0, spawn.Length)]);

            go.transform.localPosition = Vector2.zero;

            Debug.Log(i);

        }
    }
    public void Life(int vidas)
    {
        currentLife -= vidas;

        if (currentLife == 0)
        {
            Debug.Log("Game Over");
            Debug.Log(currentLife);
            Time.timeScale = 0;
            menuGameOver.SetActive(true);
        }
        string lifes;
        lifes = currentLife.ToString();
        vidaEnPantalla.text = "life=" + lifes;
    }
    void ControlDeDificultad()
    {
        if (controlTime > ts && speedControler == sc)
        {
            speedControler+=0.4f;
            timeSpawn -= 0.2f;
            ts *= 1.5f;
            sc+=0.4f;
        }
        if (timeSpawn<=1)
        {
            timeSpawn = 1;
        }
    }
    void ControlDeOleadas()
    {
        if (privateTime > timeSpawn)
        {
            SpawnEnemy(1f);
            privateTime = 0;
        }
        if (privateTimeAlly>timeSpawnAlly)
        {
            SpawnAlly(1f);
            privateTimeAlly = 0;
        }
        if (privateTimeBE > timeSpawnBE)
        {
            SpawnBigEnemy(1f);
            privateTimeBE = 0;
        }

    }
    // Interfaz Grafica ------------------------------------------
    void ActualizarReloj(float tiempo)
    {
        int minutos = 0;
        int segundos = 0;
        //int milisegundos=0;
        string textoDelReloj;
        if (tiempo < 0) tiempo = 0;
        minutos = (int)tiempo / 60;
        segundos = (int)tiempo % 60;
        //milisegundos=(int)tiempo/1000;
        textoDelReloj = minutos.ToString("00") + ":" + segundos.ToString("00");//+ ":" + milisegundos.ToString("00");
        reloj.text = textoDelReloj;

    }
    public void PointsInScreen(float gamepoints)
    {
        string pnt;
        string pntGO;
        pnt = gamepoints.ToString("000");
        pntGO = gamepoints.ToString() + " points";
        puntaje.text = pnt;
        puntajePantallaGO.text = pntGO;
    }
    public void RestartButton()
    {
        points = 0;
        SceneManager.LoadScene("Nivel1");
        Time.timeScale = 1;
    }
    public void PauseButton()
    {
        estaPausado = !estaPausado;
        if (estaPausado)
        {
            menuPaused.SetActive(true);
        }
        else
        {
            menuPaused.SetActive(false);
        }
    }
    public void ExitButton(int accion)
    {

        //SceneManager.LoadScene(0);
        switch (accion)
        {
            case 1:
                warningText.SetActive(true);
                break;
            case 2:
                SceneManager.LoadScene(0);
                break;
            case 3:
                warningText.SetActive(false);
                break;
            default:

                break;
        }
    }



    public void VolumenButton(int accion)
    {
        switch (accion)
        {
            case 1:
                soundMenu.volume = 0;
                soundOn.SetActive(false);
                soundOff.SetActive(true);
                break;
            case 2:
                soundMenu.volume = 1;
                soundOn.SetActive(true);
                soundOff.SetActive(false);
                break;
            default:
                break;
        }
    }
    public void AllyWarning()
    {
        allyWarning.SetActive(true);
    }
}

