using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementAlly : MonoBehaviour
{
    public Transform character;
    public float velocidad;
    public float direccion;
    public Vector2[] pos;
    int indice;
    private Vector2 left, rigth, limit;
    int enemyLife;
    Animator animacion;
    // Codigo --------------------------------------------------------
    private void Start()
    {
        animacion = GetComponent<Animator>();
        character = GetComponent<Transform>();
        pos = new Vector2[] { new Vector2(0, 7), new Vector2(0, 4), new Vector2(0, 2), new Vector2(0, 0), new Vector2(0, -2), new Vector2(0, -4), new Vector2(0, -6), new Vector2(0, -12) };
        left = new Vector2(-3.6f, 0);
        rigth = new Vector2(3.6f, 0);
        limit = new Vector2(0, -9f);
        enemyLife = 1;
    }
    private void Update()
    {
        if (GameManager.estaPausado)
        {
            return;
        }
        ReglasDeMovimiento();
        Movimiento(direccion);
        Point();

    }
    private void OnMouseDown()
    {
        if (OpcionesDeJuego.allyWarning==true)
        {
            GameManager.Instance.AllyWarning();
            GameManager.estaPausado = true;
            OpcionesDeJuego.allyWarning = false;
        }
        else
        {
        EnemyLife(1);
        GameManager.Instance.points --;
        GameManager.Instance.PointsInScreen(GameManager.Instance.points);

        }
        if (GameManager.estaPausado)
        {
            return;
        }

    }
    public void Movimiento(float numero)
    {
        character.Translate(new Vector2(numero, -1f) * velocidad * Time.deltaTime * GameManager.Instance.speedControler);
    }
    public void Point()
    {
        if (transform.position.y <= limit.y)
        {
            GameManager.Instance.Life(0);
            Destroy(gameObject);
        }
    }
    public void EnemyLife(int unidad)
    {
        enemyLife -= unidad;
        if (enemyLife == 0)
        {
            animacion.SetBool("dead", true);
            velocidad = 0;
        }
    }
    void ReglasDeMovimiento()
    {
        if (transform.position.x < left.x)
        {
            direccion = 1f;
        }
        else if (transform.position.x > rigth.x)
        {
            direccion = -1;
        }
        if (transform.position.y < pos[indice].y)
        {
            direccion = Random.Range(-1, 2);
            indice++;
        }
    }
    public void DestroiEnemy()
    {
        GameManager.Instance.Life(3);
        Destroy(gameObject);
    }
}
